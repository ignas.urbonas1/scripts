package main

import (
	"fmt"
	"os"
	"strings"

	"approve-credit-application/kafkaeventproducer"

	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"gopkg.in/urfave/cli.v1"
)

func main() {
	app := cli.NewApp()
	app.Name = "approve_credit_application"
	app.Usage = "Decision message to Approve Credit Application.\nE.g. `./approve_credit_application -e qa -a 65d67263-c946-4ce9-9974-914b0313b15d`\nDon't forget to add missing fields in .env.dev, .env.qa."
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "application_id, a",
			Value: "",
			Usage: "Set Credit application ID",
		},
		cli.StringFlag{
			Name:  "env, e",
			Value: "dev",
			Usage: "Set env dev, qa",
		},
	}
	app.Action = func(c *cli.Context) error {
		app_id := strings.ToLower(c.GlobalString("application_id"))
		env := strings.ToLower(c.GlobalString("env"))

		if isValidUUID(app_id) == false {
			return cli.NewExitError("Application id is missing or not UUID format", 1)
		}
		cfg, err := createCFG(env)
		if err != nil {
			fmt.Printf("Error creating config: %s\n", err)
			return cli.NewExitError("Config error. Hint: double check if .env.dev, .env.qa files are present in root dir with all fields set", 2)
		}
		// Send to Kafka
		err = kafkaeventproducer.ProduceApplicationApproveMessage(cfg, app_id)
		if err != nil {
			fmt.Printf("Error producing message: %s\n", err)
			return cli.NewExitError("Kafka producer error", 3)
		}
		fmt.Println("Message sent to Kafka")
		return nil
	}
	app.Run(os.Args)
}

func isValidUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}

func createCFG(env string) (*kafkaeventproducer.KafkaConfig, error) {
	cfg := kafkaeventproducer.NewKafkaConfig()
	var err error
	if env == "qa" {
		cfg.Namespace = "qa"
		err = godotenv.Load(".env.qa")
	} else {
		cfg.Namespace = "dev"
		err = godotenv.Load(".env.dev")
	}
	if err != nil {
		return nil, err
	}
	err = cfg.SetKakaCFG()
	return &cfg, err
}
