package kafkaeventproducer

import (
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/timestamppb"

	messagingv1 "git.curve.tools/tech/schemas/build/go/messaging/credit/v1"
	sharedamountv1 "git.curve.tools/tech/schemas/build/go/shared/amount/v1"
	sharedcreditv1 "git.curve.tools/tech/schemas/build/go/shared/credit/v1"

	"go.curve.tools/app/v3"
	"go.curve.tools/driver/v9/kafka"
)

func ProduceApplicationApproveMessage(cfg *KafkaConfig, applicationID string) error {
	topic, err := kafka.CreateTopicName(cfg.Namespace, "credit", cfg.ApproveCreditApplicationTopic, "1")
	if err != nil {
		fmt.Printf("Failed to create Kafka topic name: %s\n", err)
		return err
	}

	kafkaHosts := []string{fmt.Sprintf("%s:%s", cfg.KAFKA_HOSTS, cfg.KAFKA_PORT)}
	config := &kafka.ProducerConfig{
		Addresses:    kafkaHosts,
		Topic:        topic,
		ProducerMode: kafka.ProducerModeSynchronous,
	}

	p, err := kafka.NewProducer(context.Background(), &app.Service{}, config)
	if err != nil {
		fmt.Printf("Failed to create Kafka producer: %s\n", err)
		return err
	}

	defer p.Close()

	value := decisionApprovedValue(applicationID)

	return p.WriteMessages(context.Background(), kafka.Message{
		Key:   []byte(applicationID),
		Value: value,
	})
}

func decisionApprovedValue(applicationID string) *messagingv1.DecisionApproved {
	return &messagingv1.DecisionApproved{
		ApplicationId: applicationID,
		Terms: []*sharedcreditv1.Term{
			{
				TermMonths: 3,
				Rate: &sharedamountv1.Decimal{
					MinorUnits: 400,
					Exponent:   2,
				},
			},
			{
				TermMonths: 6,
				Rate: &sharedamountv1.Decimal{
					MinorUnits: 500,
					Exponent:   2,
				},
			},
			{
				TermMonths: 9,
				Rate: &sharedamountv1.Decimal{
					MinorUnits: 600,
					Exponent:   2,
				},
			},
			{
				TermMonths: 12,
				Rate: &sharedamountv1.Decimal{
					MinorUnits: 700,
					Exponent:   2,
				},
			},
		},
		OverrideReason: "approved message published manually (via script)",
		CreatedAt:      timestamppb.Now(),
		AffordabilityDecision: &messagingv1.AffordabilityDecision{
			CreditAmount: &sharedamountv1.Amount{
				Currency:   "GBP",
				Exponent:   2,
				MinorUnits: 150000,
			},
		},
	}
}
