package kafkaeventproducer

import (
	"errors"
	"os"
)

const (
	KAFKA_API_KEY              = "KAFKA_API_KEY"
	KAFKA_API_SECRET           = "KAFKA_API_SECRET"
	KAFKA_HOSTS                = "KAFKA_HOSTS"
	KAFKA_PORT                 = "KAFKA_PORT"
	SCHEMA_REGISTRY_API_KEY    = "SCHEMA_REGISTRY_API_KEY"
	SCHEMA_REGISTRY_API_SECRET = "SCHEMA_REGISTRY_API_SECRET"
	SCHEMA_REGISTRY_HOST       = "SCHEMA_REGISTRY_HOST"
)

type KafkaConfig struct {
	Namespace                     string
	ServiceName                   string
	ApproveCreditApplicationTopic string
	KAFKA_API_KEY                 string
	KAFKA_API_SECRET              string
	KAFKA_HOSTS                   string
	KAFKA_PORT                    string
	SCHEMA_REGISTRY_API_KEY       string
	SCHEMA_REGISTRY_API_SECRET    string
	SCHEMA_REGISTRY_HOST          string
}

func NewKafkaConfig() KafkaConfig {
	cfg := KafkaConfig{
		Namespace:                     "",
		ServiceName:                   "local-approval-test",
		ApproveCreditApplicationTopic: "decision-approved",
		KAFKA_API_KEY:                 "",
		KAFKA_API_SECRET:              "",
		KAFKA_HOSTS:                   "",
		KAFKA_PORT:                    "9092",
		SCHEMA_REGISTRY_API_KEY:       "",
		SCHEMA_REGISTRY_API_SECRET:    "",
		SCHEMA_REGISTRY_HOST:          "",
	}
	return cfg
}

func (cfg *KafkaConfig) SetKakaCFG() error {
	val, ok := os.LookupEnv(KAFKA_API_KEY)
	if !ok {
		return errors.New("Missing KAFKA_API_KEY")
	}
	cfg.KAFKA_API_KEY = val

	val, ok = os.LookupEnv(KAFKA_API_SECRET)
	if !ok {
		return errors.New("Missing KAFKA_API_SECRET")
	}
	cfg.KAFKA_API_SECRET = val

	val, ok = os.LookupEnv(KAFKA_HOSTS)
	if !ok {
		return errors.New("Missing KAFKA_HOSTS")
	}
	cfg.KAFKA_HOSTS = val

	val, ok = os.LookupEnv(KAFKA_PORT)
	if !ok {
		return errors.New("Missing KAFKA_PORT")
	}
	cfg.KAFKA_PORT = val

	val, ok = os.LookupEnv(SCHEMA_REGISTRY_HOST)
	if !ok {
		return errors.New("Missing SCHEMA_REGISTRY_HOST")
	}
	cfg.SCHEMA_REGISTRY_HOST = val

	val, ok = os.LookupEnv(SCHEMA_REGISTRY_API_KEY)
	if !ok {
		return errors.New("Missing SCHEMA_REGISTRY_API_KEY")
	}
	cfg.SCHEMA_REGISTRY_API_KEY = val

	val, ok = os.LookupEnv(SCHEMA_REGISTRY_API_SECRET)
	if !ok {
		return errors.New("Missing SCHEMA_REGISTRY_API_SECRET")
	}
	cfg.SCHEMA_REGISTRY_API_SECRET = val
	return nil
}
