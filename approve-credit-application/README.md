Usage:

1. Create two files `.env.dev` and `.env.qa` in root dir.
2. Add env variables for both environments (or the one which will be used) in .env.* files. Required fields:
	KAFKA_API_KEY=""
	KAFKA_API_SECRET=""
	KAFKA_HOSTS=""
	KAFKA_PORT="9092"
	SCHEMA_REGISTRY_API_KEY=""
	SCHEMA_REGISTRY_API_SECRET=""
	SCHEMA_REGISTRY_HOST=""
3. Turn on non-prod VPN.
4. Run script by providing environment and credit application id. Examples:
	`./approve-credit-application  -e qa -a 6c356e10-fc72-4bd4-993c-2aceec52f4f1`
	`./approve-credit-application  -e dev -a 6c356e10-fc72-4bd4-993c-2aceec52f4f1`
