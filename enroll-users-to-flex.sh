#!/bin/bash
set -e

# Usage of the following script
# UK Full Flex in DEV `./enroll-users-to-flex.sh dev uk e775b3c6-2179-4c20-a55c-34a5802a4719`
# UK Full Flex in QA `./enroll-users-to-flex.sh qa uk e775b3c6-2179-4c20-a55c-34a5802a4719`
# UK Faafs in DEV `./enroll-users-to-flex.sh dev faafs_uk e775b3c6-2179-4c20-a55c-34a5802a4719`
# UK Faafs in QA `./enroll-users-to-flex.sh qa faafs_uk e775b3c6-2179-4c20-a55c-34a5802a4719`
# UK Faafs Credit Bins in DEV `./enroll-users-to-flex.sh dev faafs_credit_bins_uk e775b3c6-2179-4c20-a55c-34a5802a4719`
# UK Faafs Credit Bins in QA `./enroll-users-to-flex.sh qa faafs_credit_bins_uk e775b3c6-2179-4c20-a55c-34a5802a4719`
# LT Full Flex in DEV `./enroll-users-to-flex.sh dev lt e775b3c6-2179-4c20-a55c-34a5802a4719`
# LT Full Flex in QA `./enroll-users-to-flex.sh qa lt e775b3c6-2179-4c20-a55c-34a5802a4719`
# UK Trial in DEV `./enroll-users-to-flex.sh dev uk_trial e775b3c6-2179-4c20-a55c-34a5802a4719`
# UK Trial in QA `./enroll-users-to-flex.sh qa uk_trial e775b3c6-2179-4c20-a55c-34a5802a4719`

ENV="$1"
if [[ "$ENV" != "dev" && "$ENV" != "qa" ]]; then
  echo "Environment should either be dev or qa">&2
  exit 1
fi

TYPE="$2"
if [[ "$TYPE" != "uk" && "$TYPE" != "lt" && "$TYPE" != "uk_trial" && "$TYPE" != "faafs_uk" && "$TYPE" != "faafs_credit_bins_uk" ]]; then
  echo "Type should either be uk or lt or uk_trial" >&2
  exit 1
fi

ACCOUNT_ID="$3"
if [ -z "$ACCOUNT_ID" ]; then
      echo "ACCOUNT_ID is empty" >&2
      exit 1
fi

ENDPOINT=""
ACCESS_TOKEN=""
PRODUCT_TYPE=0
PRODUCT_NAME=""
PRODUCT_ID=""

case "$ENV" in
   "dev") ENDPOINT="https://credit-event-producer.dev.crvos.io/produce/customer/prescreened"
   ACCESS_TOKEN="39KQkORxMtxZS4JUBHq--Xn_8QeqyGTJFWnshd1JxV4"
   ;;
   "qa") ENDPOINT="https://credit-event-producer.qa.crvos.io/produce/customer/prescreened"
   ACCESS_TOKEN="9nhgTJKuaxnWj3tLtBZD8i0iLiBEEzexmEsDrimpljE"
   ;;
esac

case "$TYPE" in
    "faafs_uk")  PRODUCT_TYPE=5
          PRODUCT_NAME="CURVE-FLEX-FAAFS-GBP-V1"
          case "$ENV" in
             "dev") PRODUCT_ID="4816ddb1-faf4-4752-9ec9-1a76ad12b8e7"
             ;;
             "qa") PRODUCT_ID="4816ddb1-faf4-4752-9ec9-1a76ad12b8e7"
             ;;
          esac
    ;;
    "faafs_credit_bins_uk")  PRODUCT_TYPE=7
          PRODUCT_NAME="CURVE-FLEX-FAAFS-CREDIT-BIN-GBP-V1"
          case "$ENV" in
             "dev") PRODUCT_ID="ffbd0d03-92df-450b-83f0-cc4fea61d02e"
             ;;
             "qa") PRODUCT_ID="ffbd0d03-92df-450b-83f0-cc4fea61d02e"
             ;;
          esac
    ;;
    "uk")  PRODUCT_TYPE=2
        PRODUCT_NAME="CURVE-FLEX-V1"
        case "$ENV" in
            "dev") PRODUCT_ID="e00ab911-f4f2-49e9-97e7-08fd20ca89a6"
            ;;
            "qa") PRODUCT_ID="4abb9751-f607-48c8-8329-01c9340c8616"
            ;;
        esac
   ;;
   "uk_trial")  PRODUCT_TYPE=4
          PRODUCT_NAME="CURVE-FLEX-TRIAL-GBP-V1"
          case "$ENV" in
             "dev") PRODUCT_ID="0812f9f7-1dc8-4e67-89c9-2f0835be6400"
             ;;
             "qa") PRODUCT_ID="3e0025ed-24c1-4b66-8571-32cc622d034f"
             ;;
          esac
   ;;
   "lt")  PRODUCT_TYPE=2
          PRODUCT_NAME="CURVE-FLEX-EUR-LT-V1"
          case "$ENV" in
             "dev") PRODUCT_ID="5f930b82-4f04-476e-95d8-d96b2977e4cc"
             ;;
             "qa") PRODUCT_ID="cd35014e-58ee-45c8-9d17-e553d3e38417"
             ;;
          esac
   ;;
esac


curl --location --request POST "$ENDPOINT" \
--header 'Authorization: '"$ACCESS_TOKEN"'' \
--header 'Content-Type: application/json' \
--data-raw '{
                       "account_id": "'"$ACCOUNT_ID"'",
                       "outcome":1,
                       "product_name":"'"$PRODUCT_NAME"'",
                       "product_type":'$PRODUCT_TYPE',
                       "product_id":"'"$PRODUCT_ID"'"
            }
' \
-i
