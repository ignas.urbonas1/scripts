package main

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"
	"gopkg.in/urfave/cli.v1"
)

const (
	snpl_uk        = "snpl_uk"
	snpl_lt        = "snpl_lt"
	snpl_trial_uk  = "snpl_trial_uk"
	faafs_uk       = "faafs_uk"
	credit_bins_uk = "credit_bins_uk"
)

type product struct {
	ID   string
	Name string
	Type uint64
}

func main() {
	app := cli.NewApp()
	app.Name = "create_csv_for_flex_enrolment"
	app.Usage = "Create csv file for airflow to enrol credti customer.\nE.g. `./create_csv_for_flex_enrolment -p credit_bins_uk -a 65d67263-c946-4ce9-9974-914b0313b15d`"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "product, p",
			Value: "",
			Usage: "Credit Product: snpl_uk, snpl_lt, snpl_trial_uk, faafs_uk, credit_bins_uk",
		},
		cli.StringFlag{
			Name:  "acc_id, a",
			Value: "",
			Usage: "Set Curve account ID",
		},
	}
	app.Action = func(c *cli.Context) error {
		product := strings.ToLower(c.GlobalString("product"))
		account_id := strings.ToLower(c.GlobalString("acc_id"))

		if isValidUUID(account_id) == false {
			return cli.NewExitError("Account id is missing or not UUID format", 2)
		}
		p, err := mapProduct(product)
		if err != nil {
			return cli.NewExitError(err, 4)
		}
		p.print()
		er := p.createCSV(account_id, product)
		if er != nil {
			return cli.NewExitError(er, 5)
		}
		fmt.Printf("CSV file output for Account %s was successfully generated!\nNow you can upload to google cloud. Link is in the HowTo doc https://imaginecurve.atlassian.net/wiki/spaces/CRED/pages/4226809969/Credit+BINs+-+Enable.\n", account_id)
		return nil
	}
	app.Run(os.Args)
}

func (p *product) print() {
	msg := fmt.Sprintf("\nProduct id = %s\nProduct name = %s\nProduct type = %d", p.ID, p.Name, p.Type)
	fmt.Println(msg)
}

func mapProduct(p string) (*product, error) {
	switch p {
	case snpl_uk:
		return &product{
			ID:   "",
			Name: "CURVE-FLEX-V1",
			Type: 2,
		}, nil

	case snpl_lt:
		return &product{
			ID:   "",
			Name: "CURVE-FLEX-EUR-LT-V1",
			Type: 2,
		}, nil

	case snpl_trial_uk:
		return &product{
			ID:   "",
			Name: "CURVE-FLEX-TRIAL-GBP-V1",
			Type: 4,
		}, nil

	case faafs_uk:
		return &product{
			ID:   "4816ddb1-faf4-4752-9ec9-1a76ad12b8e7",
			Name: "CURVE-FLEX-FAAFS-GBP-V1",
			Type: 5,
		}, nil

	case credit_bins_uk:
		return &product{
			ID:   "ffbd0d03-92df-450b-83f0-cc4fea61d02e",
			Name: "CURVE-FLEX-FAAFS-CREDIT-BIN-GBP-V1",
			Type: 5,
		}, nil

	default:
		msg := fmt.Sprintf("Specified product do not exist %s", p)
		return nil, errors.New(msg)
	}
}

func (p *product) createCSV(acc_id string, product string) error {
	now := time.Now()
	lastAccidSymbol := acc_id[len(acc_id)-2:]
	filename := fmt.Sprintf("%s_%s_user%s", now.Format("2006-01-02"), product, lastAccidSymbol)
	fmt.Printf("filename = %s\n", filename)

	if _, err := os.Stat(filename); err == nil {
		er := os.Remove(filename)
		if er != nil {
			return errors.New("File exists! Cannot remove!")
		}
	}
	csvFile, err := os.Create(filename)
	if err != nil {
		msg := fmt.Sprintf("cannot create file %s\n", filename)
		return errors.New(msg)
	}
	defer csvFile.Close()

	content := fmt.Sprintf("curve_account_id,outcome,product_name,product_type,product_id\n%s,1,%s,%d,%s", acc_id, p.Name, p.Type, p.ID)
	_, er := csvFile.WriteString(content)
	if er != nil {
		return errors.New("Failed to write content to the file!")
	}
	return nil
}

func isValidUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}
