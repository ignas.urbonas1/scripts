echo "💩 DerivedData"
rm -rf ~/Library/Developer/Xcode/DerivedData
echo "💩 Carthage cache"
rm -rf ~/Library/Caches/org.carthage.CarthageKit
echo "💩 remove SwiftGen just in case but this didn't help me..."
mint uninstall swiftgen
echo "Punish mint packages with 😡 rage..."
rm -r ~/.mint

echo "Bootstrap Carthage dependencies"
bundle exec fastlane bootstrap_carthage

echo "Clean tuist"
tuist clean
echo "Update tuist tools"
bundle exec fastlane run tuist command:up
echo "Fetch project dependencies just in case"
tuist dependencies fetch
echo "Generate & open project"
tuist generate --open
