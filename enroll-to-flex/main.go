package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	"gopkg.in/urfave/cli.v1"
)

const (
	snpl_uk        = "snpl_uk"
	snpl_lt        = "snpl_lt"
	snpl_trial_uk  = "snpl_trial_uk"
	faafs_uk       = "faafs_uk"
	credit_bins_uk = "credit_bins_uk"

	env_dev = "dev"
	env_qa  = "qa"

	endpoint_dev     = "https://credit-event-producer.dev.crvos.io/produce/customer/prescreened"
	access_token_dev = "39KQkORxMtxZS4JUBHq--Xn_8QeqyGTJFWnshd1JxV4"
	endpoint_qa      = "https://credit-event-producer.qa.crvos.io/produce/customer/prescreened"
	access_token_qa  = "9nhgTJKuaxnWj3tLtBZD8i0iLiBEEzexmEsDrimpljE"
)

type product struct {
	ID   string
	Name string
	Type uint64
}

func main() {
	app := cli.NewApp()
	app.Name = "enrol_to_flex"
	app.Usage = "Enrol user to credit products.\nE.g. `./enrol_to_flex -p credit_bins_uk -e qa -a 1515be1c-8d60-4a85-a3f3-fb53c1f3110a`"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "env, e",
			Value: "dev",
			Usage: "Select qa or dev environment",
		},
		cli.StringFlag{
			Name:  "product, p",
			Value: "",
			Usage: "Credit Product: snpl_uk, snpl_lt, snpl_trial_uk, faafs_uk, credit_bins_uk",
		},
		cli.StringFlag{
			Name:  "acc_id, a",
			Value: "",
			Usage: "Set Curve account ID",
		},
	}
	app.Action = func(c *cli.Context) error {
		env := strings.ToLower(c.GlobalString("env"))
		product := strings.ToLower(c.GlobalString("product"))
		account_id := strings.ToLower(c.GlobalString("acc_id"))

		if account_id == "" {
			return cli.NewExitError("Account id is missing", 2)
		}
		if env != env_dev && env != env_qa {
			return cli.NewExitError("environment is unknown", 3)
		}
		p, err := mapProduct(product, env)
		if err != nil {
			return cli.NewExitError(err, 4)
		}
		p.print()
		er := p.enrol(account_id, env)
		if er != nil {
			return cli.NewExitError(er, 5)
		}
		fmt.Printf("Account %s was successfully made eligible!\n", account_id)
		return nil
	}
	app.Run(os.Args)
}

func mapProduct(p string, env string) (*product, error) {
	switch p {
	case snpl_uk:
		var id string
		switch env {
		case env_dev:
			id = "e00ab911-f4f2-49e9-97e7-08fd20ca89a6"
		case env_qa:
			id = "4abb9751-f607-48c8-8329-01c9340c8616"
		}
		return &product{
			ID:   id,
			Name: "CURVE-FLEX-V1",
			Type: 2,
		}, nil

	case snpl_lt:
		var id string
		switch env {
		case env_dev:
			id = "5f930b82-4f04-476e-95d8-d96b2977e4cc"
		case env_qa:
			id = "cd35014e-58ee-45c8-9d17-e553d3e38417"
		}
		return &product{
			ID:   id,
			Name: "CURVE-FLEX-EUR-LT-V1",
			Type: 2,
		}, nil

	case snpl_trial_uk:
		var id string
		switch env {
		case env_dev:
			id = "0812f9f7-1dc8-4e67-89c9-2f0835be6400"
		case env_qa:
			id = "3e0025ed-24c1-4b66-8571-32cc622d034f"
		}
		return &product{
			ID:   id,
			Name: "CURVE-FLEX-TRIAL-GBP-V1",
			Type: 4,
		}, nil

	case faafs_uk:
		return &product{
			ID:   "4816ddb1-faf4-4752-9ec9-1a76ad12b8e7",
			Name: "CURVE-FLEX-FAAFS-GBP-V1",
			Type: 5,
		}, nil

	case credit_bins_uk:
		return &product{
			ID:   "ffbd0d03-92df-450b-83f0-cc4fea61d02e",
			Name: "CURVE-FLEX-FAAFS-CREDIT-BIN-GBP-V1",
			Type: 7,
		}, nil

	default:
		msg := fmt.Sprintf("Specified product do not exist %s", p)
		return nil, errors.New(msg)
	}
}

func (p *product) print() {
	msg := fmt.Sprintf("\nProduct id = %s\nProduct name = %s\nProduct type = %d", p.ID, p.Name, p.Type)
	fmt.Println(msg)
}

func (p *product) enrol(acc_id string, env string) error {
	var url string
	var token string
	switch env {
	case env_dev:
		url = endpoint_dev
		token = access_token_dev
	case env_qa:
		url = endpoint_qa
		token = access_token_qa
	}
	fmt.Printf("Request endpoint = %s\n", url)

	params := map[string]interface{}{
		"account_id":   acc_id,
		"outcome":      1,
		"product_name": p.Name,
		"product_type": p.Type,
		"product_id":   p.ID,
	}
	body, err := json.Marshal(params)
	if err != nil {
		return err
	}
	reqBody := bytes.NewBuffer(body)
	req, err := http.NewRequest("POST", url, reqBody)
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", token)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusCreated {
		fmt.Print(req)
		msg := fmt.Sprintf("Wrong Http Status received, status = %d (should be Created 201)\n", res.StatusCode)
		return errors.New(msg)
	}
	return nil
}
