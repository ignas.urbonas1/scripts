import UIKit


struct Flag {
    let key: String
}
let flags: [Flag] = (1...100000).map { .init(key: "title_\($0)") }
let searchTerm = ""

// Impl

let start = Date.timeIntervalSinceReferenceDate

//var flagsTemp = flags
//if !searchTerm.isEmpty {
    _ = flags.filter { flag in
        searchTerm.isEmpty || flag.key.lowercased().contains(searchTerm.lowercased())
    }
//}

let stop = Date.timeIntervalSinceReferenceDate


print("Time taken: ", (stop - start))
