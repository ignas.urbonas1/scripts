import UIKit
import Combine

//let doubleValue = Double(100.23456654323456789876543)
//doubleValue.formatted()
//
//
//print(doubleValue)
//print(doubleValue.formatted())

///-----------------------

//public enum AsyncError: Error {
//    case finishedWithoutValue
//}
//
//// Solution is taken from https://medium.com/geekculture/from-combine-to-async-await-c08bf1d15b77
//public extension AnyPublisher {
//    func async(completeAfterFirstValue: Bool = true) async throws -> Output {
//        try await withCheckedThrowingContinuation { continuation in
//            var finishedWithoutValue = true
//            var cancellable: AnyCancellable?
//            var publisher = self
//            if completeAfterFirstValue {
//                publisher = first().eraseToAnyPublisher()
//            }
//            cancellable = publisher
//                .sink { result in
//                    switch result {
//                    case .finished:
//                        if finishedWithoutValue {
//                            continuation.resume(throwing: AsyncError.finishedWithoutValue)
//                        }
//                    case let .failure(error):
//                        continuation.resume(throwing: error)
//                    }
//                    cancellable?.cancel()
//                } receiveValue: { value in
//                    finishedWithoutValue = false
//                    continuation.resume(with: .success(value))
//                }
//        }
//    }
//}
//
//let numbers = (-10...10)
//var cancellable: AnyCancellable?
////cancellable = numbers.publisher
////    .sink(receiveCompletion: { _ in print("Done")}, receiveValue: { print($0) })
//
//
//let pass = PassthroughSubject<Int, Never>()
////cancellable = pass
////    .sink(receiveCompletion: { _ in print("Done")}, receiveValue: { print($0) })
//
////for i in -10...10 {
////    pass.send(i)
////}
//let publisher = AnyPublisher(pass)
////Task {
////    let result = try await publisher.async()
////    pass.send(110)
////}
//
//try await withThrowingTaskGroup(of: Bool.self) { group in
//    group.addTask{
//        let result = try await publisher.async(completeAfterFirstValue: true)
//        print("result = \(result)")
//        return true
//    }
//    group.addTask {
//        for i in -10...10 {
//            print(i)
//            pass.send(i)
//        }
//        return true
//    }
//    
//    for try await i in group {
//        print(i)
//    }
//    return true
//}
//
//print("Done")
//let dateFormatter = DateFormatter()
//
//var time: Date {
//    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ" // 2024-04-17T12:55:36.780918Z
//    return dateFormatter.date(from: "2024-04-17T12:55:36.780918Z")!
//}
//
//time
//
//let number = 3.145678
//
//round(number*100)/100
//String(format: "%.2f", number)
//String(format: "%.3f", number)
//
//print(number)


// Async stream
//
//let stream1 = AsyncStream { cont in
//    cont.yield(nil as Int?)
//    cont.yield(2)
//}
//
//let stream2 = AsyncStream { cont in
//    cont.yield(nil as Int?)
//    cont.yield(nil)
//}
//
//print("Async stream1")
//let stream1Event = await stream1.first(where: { $0 != nil })?.map { $0 }
//print("stream1Event = \(String(describing: stream1Event))")
//
//
//print("Async stream2")
//
//let stream2Event = await stream2.prefix(2).first(where: { $0 != nil })?.map { $0 }
//print("stream2Event = \(String(describing: stream2Event))")
//
//print("Async finished")


// ----- Features -----

//struct FeatureA_API {
//    let propertyA: () -> String // simplified factory
//}
//
//struct FeatureB_API {
//    let name: () -> String
//    let addCardFlow: () -> (() -> Void)
//}
//
//struct FeatureA {
//    static let ContainerLiveA = FeatureA_API(propertyA: { "A" })
//}
//
//struct FeatureB {
//    static let ContainerLiveB = FeatureB_API(name: { "name" }, addCardFlow: { { print("flow started")} })
//
//}
//
//let ContainerLiveA = FeatureA.ContainerLiveA
//let ContainerLiveB = FeatureB.ContainerLiveB
//
//// --- Currently we sue like this ---
//struct FeatureC {
//    
//    func doOne() {
//        print(ContainerLiveA.propertyA())
//    }
//    
//    func doAnother() {
//        print(ContainerLiveB.name())
//    }
//}
//
//let featureC = FeatureC()
//featureC.doOne()
//featureC.doAnother()
//
//// ---- Additional decoupling ----
//struct FeatureD {
//    // can also be just a protocol and struct with methods to have labels and better documentation
//    // Reduce and limit the scope by declaring what is needed for feature D and skipping else what's available in FeatureA abd FeatureB
//    struct Environment {
//        let doOneStuff: () -> String
//        let doAnotherStuff: () -> String
//    }
//    
//    static let liveD = FeatureD.Environment(
//        doOneStuff: { ContainerLiveA.propertyA() },
//        doAnotherStuff: { ContainerLiveB.name() }
//    )
//    
//    let env: Environment
//    
//    func doOne() {
//        print(ContainerLiveA.propertyA())
//    }
//    
//    func doAnother() {
//        print(ContainerLiveB.name())
//    }
//}
//
//
//let featureD = FeatureD(env: FeatureD.liveD)
//featureD.doOne()
//featureD.doAnother()




/// -----



struct Recipe {
    var title: String
}

protocol RecipeService {
    func asyncStream() -> AsyncStream<Recipe?>
}

final class RecipeServiceImpl: RecipeService {
    func asyncStream() -> AsyncStream<Recipe?> {
        AsyncStream(bufferingPolicy: .bufferingNewest(2)) { continuation in
            let cancellable = self.recipeChangesSubject.sink {
                continuation.yield($0)
            }
            continuation.onTermination = { continuation in
                cancellable.cancel()
            }
        }
    }

    func addRecipe(_ recipe: Recipe) async {
        print("add ->", recipe.title)
        recipeChangesSubject.send(recipe)
    }
  
  private let recipeChangesSubject = CurrentValueSubject<Recipe?, Never>(nil)

}

let imp = RecipeServiceImpl()
//let stream = imp.asyncStream()
//
//Task { @MainActor in
//    for try await value in stream {
//        print("streamed = ", value?.title ?? "nil")
//    }
//}

//await imp.addRecipe(.init(title: "zero"))


class VC1 {
    func start(imp: RecipeService) {
        let stream = imp.asyncStream()

        Task {
            for try await value in stream {
                print("stream[1] = ", value?.title ?? "nil")
            }
        }
    }
}

class VC2 {
    var task: Task<(), any Error>?
    
    func start(imp: RecipeService) {
        let t = Task {
            let stream = imp.asyncStream()
            for try await value in stream {
                print("streamed[2] = ", value?.title ?? "nil")
            }
        }
        self.task = t
    }
    deinit {
        task?.cancel() // <- need to cancel
        print("vc2 deinit")
    }
}

let vc1 = VC1()
var vc2: VC2? = VC2()

vc1.start(imp: imp)
vc2?.start(imp: imp)

vc2 = nil
vc2?.task?.cancel()

await imp.addRecipe(.init(title: "me"))
await imp.addRecipe(.init(title: "you"))
await imp.addRecipe(.init(title: "him"))



let stream = imp.asyncStream()
let recipe = await stream.first(where: { $0 != nil}) ?? nil

recipe?.title

CFRunLoopRun()
