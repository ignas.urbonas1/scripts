import UIKit

// MARK: - async/await

protocol Cancellable {
    func cancel()
}

struct Cancel: Cancellable {
    func cancel() {
        print("[\(String(describing: self))] Cancel")
        cancelled = true
    }
}

class CancellableHolder: @unchecked Sendable {
    var cancellable: Cancellable?
    
    deinit {
        print("CancellableHolder -> deinit")
    }
}

var cancelled = false

func api(_ completion:  @escaping (String) -> Void) -> Cancellable {
    DispatchQueue.global().async {
        for i in 1...5 {
            sleep(1)
            print("api -> \(i)")
        }
        completion("api -> Done")
        completion("api -> Done 2")

    }
    return Cancel()
}

func function(id: Int) async throws -> String {
    print("[Function] id: \(id)")
    let cancellableHolder = CancellableHolder()
    return try await withTaskCancellationHandler(
        operation: {
            print("operation -> check cancellation")
            try Task.checkCancellation()
            return try await withCheckedThrowingContinuation { continuation in
                print("operation -> check throwing continuation")
                let cancellable = api {
                    print("operation -> continuation resume")
                    continuation.resume(returning: $0)
                    cancellableHolder.cancellable?.cancel()
                }
                cancellableHolder.cancellable = cancellable
            }
        },
        onCancel: { [cancellableHolder] in
            print("onCancel -> \(String(describing: cancellableHolder.cancellable))")
            cancellableHolder.cancellable?.cancel()
        }
    )
}

// MARK: - Demo

let taskToComplete = Task {
    let result = try await function(id: 1)
    print("[TaskToComplete Result] \(result)")
    return result
}

do {
    let result = try await taskToComplete.value
} catch let error {
    print("[Error] \(error)")
}

print("--------------------------------------")
sleep(1)

let taskToCancel = Task {
    let result = try await function(id: 2)
    print("[TaskToCancel Result] -> \(result)")
    return result
}
taskToCancel.cancel()

do {
    let result = try await taskToCancel.value
} catch let error {
    print("[Error] \(error)")
}

print("--------------------------------------")
sleep(1)

print("Done")
