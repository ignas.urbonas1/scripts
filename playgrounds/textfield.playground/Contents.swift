//: A UIKit based Playground for presenting user interface
  
import SwiftUI
import PlaygroundSupport

import SwiftUI
import PlaygroundSupport

struct ContentView: View {
    @State var text = ""
    var body: some View {
        TextField("text", text: $text)
    }
}

PlaygroundPage.current.setLiveView(ContentView())
