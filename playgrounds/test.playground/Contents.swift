import UIKit

var greeting = "Hello, playground"

var elements = "abcd"

elements.removeAll(where: { $0 == "v" })



// Generate password

var psw = ""
for _ in 0...12 {
    for _ in 0...12 {
        let char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789= * [ ] { } - ? . , ; : _ ) % ( / $ # ! | & + @ '"
            .replacingOccurrences(of: " ", with: "")
            .randomElement()!
        psw += String(char)
    }
    print(psw)
    psw = ""
}


// JSON

struct AuthInfo: Decodable {
    let access_token: String
    let refresh_token: String
    let id_token: String
    let scope: String
    let expires_in: Int
    let token_type: String
}

let data = """
{
  "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJ3N1JZVjktaTc0WmxKSkZtY28wUCJ9.eyJodHRwczovL2N1cnZlLmFwcCI6eyJhaWQiOiIyMjI3MjViNy03MzM5LTQwYjUtOWIxYy1kODUyMjlhMTRkMjUiLCJhbXIiOlsib3RwIl0sImNhdCI6ImYwZWVmMGJjZjVjNmNkYmRlMmNhNWE1NDJkNGFlYzY0NjFhYjFjZTE1YjZmMGNjZDBjMzRmMzQxN2QwNGM2MjYiLCJvcCI6W119LCJpc3MiOiJodHRwczovL2F1dGguZGV2LmV1LXdlc3QtMS5jcnZvcy5pby8iLCJzdWIiOiJzbXN8NjExYmRiZDRkMWNlOWY5ZGM4OTc2YzQ5IiwiYXVkIjpbImh0dHBzOi8vZGV2LmNydm9zLmlvLyIsImh0dHBzOi8vY3VydmUtZXUtZGV2LmV1LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE3MTExMDc2MjIsImV4cCI6MTcxMTEwNzkyMiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCBhZGRyZXNzIHBob25lIHJlYWQ6cHJvZmlsZSBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIiwiYXpwIjoibWRWT0gxSHdjNVM5OFpSczQwVllqdG9kdDBONGF3RUkifQ.dWOKp3oYXSrE6SiKpNaXC6gv28KMrB1K1VPhzse87LdExllCP7rkzhiNYJeOkoV0mr8dbIEWSKNNGurZ_Civzp_yLxQYk-e0AS0k6vwbr4SKpPB-JgNHD8AshnA91Bg-kWN6SCzdKxcjk8n_2IN1RghBT31Qfrs9RDzVColleJ-JDmN2hbYUyFyQROCi36yaqrStBz5DZmCfexyE_J-oZ2bVW-u4i-EAhVsKtCRT8Q5H_jtiDKzPEcvLoKbF9Q16jNzWg5A_rGvtw44JJff70MwbGNiTnubY9iBNZ1ipCLcWf-Q3yNc7_43hBa8UxVy4DaJr_fGai1XPPMkLbam_6A",
  "refresh_token": "nqwnJuvJR6QQ7dWMt6glRlLTVpfMafg5K8QVB4WV3A_hM",
  "id_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJ3N1JZVjktaTc0WmxKSkZtY28wUCJ9.eyJodHRwczovL2N1cnZlLmFwcCI6eyJjcmVhdGUiOmZhbHNlLCJwcyI6ImNvbmZpcm1lZCJ9LCJuaWNrbmFtZSI6IiIsIm5hbWUiOiIrNDQ3NDIyMzQ2NjQzIiwicGljdHVyZSI6Imh0dHBzOi8vY2RuLmF1dGgwLmNvbS9hdmF0YXJzL2RlZmF1bHQucG5nIiwidXBkYXRlZF9hdCI6IjIwMjQtMDMtMjJUMTE6NDA6MjAuODU4WiIsInBob25lX251bWJlciI6Iis0NDc0MjIzNDY2NDMiLCJpc3MiOiJodHRwczovL2F1dGguZGV2LmV1LXdlc3QtMS5jcnZvcy5pby8iLCJhdWQiOiJtZFZPSDFId2M1Uzk4WlJzNDBWWWp0b2R0ME40YXdFSSIsImlhdCI6MTcxMTEwNzYyMiwiZXhwIjoxNzExMTE0ODIyLCJzdWIiOiJzbXN8NjExYmRiZDRkMWNlOWY5ZGM4OTc2YzQ5In0.AZ0dm73s2JzKvwhBNS61QDZJqfweZyBAch6rz0Y5CH4IWjpuyAdq2zpjEHe6ZMwZMq7VARqwOXrfCjgU1ieuV8n9Ad3MlCJSU8aW_y3DO0HBL1Qsdqa96XmGbaZh76tAnczS6ov8BpfN-Vcwc-ZnEBK9mO4TQaBMmHFhRmm3b8O-8SIR1SqyNFzdb2_2JmkkW_b6YyN1klrGbKkERZWQhozmFHV1O_piK7lqDsH8Wwv-Gr32Xf97Fj_lj9-LTR2zv1_OPHKbLN8HqB6zV_ajQNuFPN83WTd9lpyGKl7GX1iuHBrOxhQNQK__ya_kOos4DHk5pTqgTSMWVtIbKWEtXA",
  "scope": "openid profile email address phone read:profile offline_access",
  "expires_in": 300,
  "token_type": "Bearer"
}
""".data(using: .utf8)!

let authInfo = try JSONDecoder().decode(AuthInfo.self, from: data)

print(authInfo)

// -----
